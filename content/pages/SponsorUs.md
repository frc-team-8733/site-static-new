---
title: "Sponsor Us"
slug: 'sponsor-us'
url: '/sponsor-us'
---


# Sponsor US

If you or your organization would like to sponsor us, we would be pleased to have you do so. Reach out to us at mailto:sponsor@arsrobotics.org. An information packet can be found at https://bucket.arsrobotics.org/minecraftchest1/1de70116-bc73-4d36-b177-1fb1ac105f09 
