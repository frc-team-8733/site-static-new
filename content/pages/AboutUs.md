---
title: "About Us"
slug: 'about-us'
url: '/about-us'
---

![Team picture around robot at worlds, 2022](/images/TeamPictureRobot2022.jpg)

# About Us

The Athens Renaissance Robotics Team (FIRST® Robotics Competition Team #538) is comprised of 9th-12th graders from Athens Renaissance School. FIRST® (For Inspiration and Recognition of Science and Technology) is a non-profit organization dedicated to getting students involved in science, technology, engineering, and mathematics. This year, in our second season, our team will design, construct, and program a robot to perform a series of assigned tasks while engaging in outreach activities with the goal of inspiring our community to get excited about engineering.

Through the program, students are able to :


-learn mechanical skills such as metalworking, using power tools, precise measuring, industrial safety procedures, and using pneumatic components;

-learn electrical and programming skills such as soldering, power distribution, using a multimeter, and learning the basics of programming in C++ and Java;

-learn problem-solving and soft skills such as working with others, meeting deadlines, thinking logically and creatively, planning ahead, learning from experts, and persevering in the face of adversity.


